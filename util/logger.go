package util

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"runtime/debug"
	"strings"
)

func ExePath() (string, error) {
	prog := os.Args[0]
	p, err := filepath.Abs(prog)
	if err != nil {
		return "", err
	}
	fi, err := os.Stat(p)
	if err == nil {
		if !fi.Mode().IsDir() {
			return p, nil
		}
		err = fmt.Errorf("%s is directory", p)
	}
	if filepath.Ext(p) == "" {
		p += ".exe"
		fi, err := os.Stat(p)
		if err == nil {
			if !fi.Mode().IsDir() {
				return p, nil
			}
			return "", fmt.Errorf("%s is directory", p)
		}
	}
	return "", err
}

func LogError(content string) {
	exePath, err := ExePath()
	if err != nil {
		panic(err)
	}

	realPath := strings.Join(strings.Split(exePath, "\\")[:len(strings.Split(exePath, "\\"))-1], "/")

	if _, err := os.Stat(fmt.Sprintf("%s/error.log", realPath)); err == nil {
		// path/to/whatever exists
		f, _ := os.OpenFile(fmt.Sprintf("%s/error.log", realPath),
			os.O_APPEND, 0644)
		defer f.Close()
		f.WriteString(fmt.Sprintf("\n\n%s \n", content))
	} else if errors.Is(err, os.ErrNotExist) {
		// path/to/whatever does *not* exist
		f, _ := os.Create(fmt.Sprintf("%s/error.log", realPath))
		defer f.Close()
		f.WriteString(fmt.Sprintf("\n\n%s \n", content))
	}
}

func LogPanicError(r interface{}) {
	exePath, err := ExePath()
	if err != nil {
		panic(err)
	}

	realPath := strings.Join(strings.Split(exePath, "\\")[:len(strings.Split(exePath, "\\"))-1], "/")

	if _, err := os.Stat(fmt.Sprintf("%s/error.log", realPath)); err == nil {
		// path/to/whatever exists
		f, _ := os.OpenFile(fmt.Sprintf("%s/error.log", realPath),
			os.O_APPEND, 0644)
		defer f.Close()
		f.WriteString(fmt.Sprintf("\n\n%+v \n %s", r, string(debug.Stack())))
	} else if errors.Is(err, os.ErrNotExist) {
		// path/to/whatever does *not* exist
		f, _ := os.Create(fmt.Sprintf("%s/error.log", realPath))
		defer f.Close()
		f.WriteString(fmt.Sprintf("%+v \n %s", r, string(debug.Stack())))
	}
}

func LogActivity(content string) {
	exePath, err := ExePath()
	if err != nil {
		panic(err)
	}

	realPath := strings.Join(strings.Split(exePath, "\\")[:len(strings.Split(exePath, "\\"))-1], "/")

	if _, err := os.Stat(fmt.Sprintf("%s/activity.log", realPath)); err == nil {
		// path/to/whatever exists
		f, _ := os.OpenFile(fmt.Sprintf("%s/activity.log", realPath),
			os.O_APPEND, 0644)
		defer f.Close()
		f.WriteString(fmt.Sprintf("\n\n%s \n", content))
	} else if errors.Is(err, os.ErrNotExist) {
		// path/to/whatever does *not* exist
		f, _ := os.Create(fmt.Sprintf("%s/activity.log", realPath))
		defer f.Close()
		f.WriteString(fmt.Sprintf("\n\n%s \n", content))
	}
}
