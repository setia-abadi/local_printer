// Copyright 2012 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build windows
// +build windows

package main

import (
	"fmt"
	"os"
	"setia-abadi-printer/delivery/api"
	"setia-abadi-printer/global"
	"setia-abadi-printer/manager"
	"setia-abadi-printer/util"
	"strings"
	"time"

	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/debug"
)

type printerService struct {
}

func (m *printerService) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	const cmdsAccepted = svc.AcceptStop | svc.AcceptShutdown | svc.AcceptPauseAndContinue
	changes <- svc.Status{State: svc.StartPending}
	m.runApp()
	changes <- svc.Status{State: svc.Running, Accepts: cmdsAccepted}
loop:
	for {
		c := <-r
		switch c.Cmd {
		case svc.Interrogate:
			changes <- c.CurrentStatus
			time.Sleep(100 * time.Millisecond)
			changes <- c.CurrentStatus
		case svc.Stop, svc.Shutdown:
			util.LogActivity("Shutdown HTTP Server")
			break loop
		case svc.Pause:
			changes <- svc.Status{State: svc.Paused, Accepts: cmdsAccepted}
		case svc.Continue:
			changes <- svc.Status{State: svc.Running, Accepts: cmdsAccepted}
		default:
			util.LogError(fmt.Sprintf("Unexpected control request #%d", c))
		}
	}
	changes <- svc.Status{State: svc.StopPending}
	return
}

func runService(name string, isDebug bool) {
	util.LogActivity(fmt.Sprintf("starting %s service", name))
	run := svc.Run
	if isDebug {
		run = debug.Run
	}
	err := run(name, &printerService{})
	if err != nil {
		util.LogError(fmt.Sprintf("%s service failed: %v", name, err))
		return
	}
	util.LogActivity(fmt.Sprintf("%s service stopped", name))
}

func (m *printerService) runApp() {
	util.LogActivity(strings.Join(os.Args, "-"))

	path, err := util.ExePath()
	if err != nil {
		util.LogError(fmt.Sprintf("Failed to find current path: %v", err))
	}

	container := manager.NewContainer(
		path,
	)
	router := api.NewRouter(container)

	global.SetPrinterName(os.Args[2])
	go func() {
		// exeFolderPath := strings.Join(strings.Split(path, "\\")[:len(strings.Split(path, "\\"))-1], "/")

		// certFile := fmt.Sprintf("%s/%s.pem", exeFolderPath, "cert")
		// keyFile := fmt.Sprintf("%s/%s.pem", exeFolderPath, "key")

		// err := router.RunTLS(fmt.Sprintf(":%s", os.Args[1]), certFile, keyFile)
		err := router.Run(fmt.Sprintf(":%s", os.Args[1]))
		if err != nil {
			util.LogError(err.Error())
			panic(err)
		}
	}()

	util.LogActivity("HTTP Server Started")
}
