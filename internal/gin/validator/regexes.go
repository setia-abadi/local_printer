package validator

import "regexp"

var (
	splitParamsRegexString = `'[^']*'|\S+`
)

var (
	splitParamsRegex = regexp.MustCompile(splitParamsRegexString)
)
