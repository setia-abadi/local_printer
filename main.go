// Copyright 2012 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build windows
// +build windows

// Example service program that beeps.
//
// The program demonstrates how to create Windows service and
// install / remove it on a computer. It also shows how to
// stop / start / pause / continue any service, and how to
// write to event log. It also shows how to use debug
// facilities available in debug package.
package main

import (
	"fmt"
	"log"
	"os"
	"setia-abadi-printer/global"
	"setia-abadi-printer/util"
	"strconv"
	"strings"
	"time"

	"golang.org/x/sys/windows/svc"
)

func usage(errmsg string) {
	fmt.Fprintf(os.Stderr,
		"%s\n\n"+
			"usage: %s <command>\n"+
			"       where <command> is one of\n"+
			"       install <port> <printer_name>, remove.\n",
		"	    <port> must be between 1000 and 10000\n"+
			errmsg, os.Args[0])
	os.Exit(2)
}

func main() {

	svcName := global.GetConfig().ServiceName

	inService, err := svc.IsWindowsService()
	if err != nil {
		log.Fatalf("failed to determine if we are running in service: %v", err)
	}

	if inService {
		runService(svcName, false)
		return
	}

	if len(os.Args) < 2 {
		usage("no command specified")
	}

	cmd := strings.ToLower(os.Args[1])
	switch cmd {
	case "install":
		if len(os.Args) < 4 {
			usage("required port and printer_name arguments are missing")
		}

		if !isPortValid(os.Args[2]) {
			usage("invalid port")
		}

		if !isSecretValid(os.Args[3]) {
			usage("invalid printer_name")
		}

		// certFile := fmt.Sprintf("./%s.pem", "cert")
		// if _, err := os.Stat(certFile); errors.Is(err, os.ErrNotExist) {
		// 	usage(err.Error())
		// }

		// keyFile := fmt.Sprintf("./%s.pem", "key")
		// if _, err := os.Stat(keyFile); errors.Is(err, os.ErrNotExist) {
		// 	usage(err.Error())
		// }

		// if service is installed and running, stop and remove it
		services := mustListServices()
		for _, service := range services {
			if service == svcName {
				remove(svcName)
				break
			}
		}

		install(svcName)
	case "remove":
		remove(svcName)
	default:
		usage(fmt.Sprintf("Invalid command %s", cmd))
	}
}

func isPortValid(port string) bool {
	if port == "" {
		return false
	} else {
		portInt, err := strconv.Atoi(port)
		if err != nil {
			return false
		} else {
			if portInt < 1000 || portInt > 10000 {
				return false
			}
		}
	}

	return true
}

func isSecretValid(secret string) bool {
	return secret != ""
}

func remove(svcName string) {
	err := controlService(svcName, svc.Stop, svc.Stopped)
	if err != nil {
		util.LogError(fmt.Sprintf("Failed to stop %s: %v", svcName, err))
	}

	err = removeService(svcName)
	if err != nil {
		util.LogError(fmt.Sprintf("Failed to remove %s: %v", svcName, err))
	}
}

func install(svcName string) {
	err := installService(svcName, "Go HTTP Server", os.Args[2], os.Args[3])
	if err != nil {
		util.LogError(fmt.Sprintf("Failed to install %s: %v", svcName, err))
	}

	time.Sleep(1 * time.Second)
	err = startService(svcName)
	if err != nil {
		util.LogError(fmt.Sprintf("Failed to start %s: %v", svcName, err))
	}
}
