package middleware

import (
	"context"
	"setia-abadi-printer/util"

	"github.com/gin-gonic/gin"
)

type hmacSecretKeyString string

var hmacSecretKey = hmacSecretKeyString(util.MustGenerateRandomString(64))

func SignatureMiddleware(router gin.IRouter) {
	router.Use(func(ctx *gin.Context) {
		signature := ctx.Request.Header.Get("X-Signature")
		if signature != "" {
			newCtx := context.WithValue(ctx.Request.Context(), hmacSecretKey, signature)
			ctx.Request = ctx.Request.WithContext(newCtx)
		}

		ctx.Next()
	})
}

func GetSignature(ctx context.Context) string {
	if v := ctx.Value(hmacSecretKey); v != nil {
		return v.(string)
	}
	return ""
}
