package middleware

import (
	"log"
	"runtime/debug"
	"setia-abadi-printer/delivery/dto_response"
	"setia-abadi-printer/manager"
	"setia-abadi-printer/util"

	"github.com/gin-gonic/gin"
)

func PanicHandlers(router gin.IRouter, container *manager.Container) {
	router.Use(func(ctx *gin.Context) {
		defer func() {
			if r := recover(); r != nil {
				util.LogPanicError(r)

				switch v := r.(type) {
				case dto_response.ErrorResponse:
					ctx.AbortWithStatusJSON(v.Code, v)
					return
				case error:
					log.Printf("Captured Error: %s\n", v.Error())
				default:
					log.Printf("Unhandled ERR TYPE %T, Content: %+v\n", v, v)
				}

				debug.PrintStack()

				v := dto_response.NewInternalServerErrorResponse()
				ctx.AbortWithStatusJSON(v.Code, v)
			}
		}()

		ctx.Next()
	})
}
