package use_case

import (
	"setia-abadi-printer/delivery/dto_request"
	"setia-abadi-printer/delivery/dto_response"
	"setia-abadi-printer/global"
	"sync"

	"github.com/alexbrainman/printer"
)

type PrinterUseCase interface {
	GetPrinterList() []string
	Print(request dto_request.PrinterPrintRequest)
}

type printerUseCase struct {
	printerMutex map[string]*sync.Mutex
}

func (u printerUseCase) mustValidatePrinterName(name string) {
	printerList, err := printer.ReadNames()
	if err != nil {
		panic(err)
	}

	valid := false
	for i := 0; i < len(printerList); i++ {
		if printerList[i] == name {
			valid = true
			break
		}
	}

	if !valid {
		panic(dto_response.ErrPrinterNameNotAvailable)
	}
}

func (u *printerUseCase) GetPrinterList() []string {
	printerList, err := printer.ReadNames()

	if err != nil {
		panic(err)
	}

	return printerList
}

func (u *printerUseCase) execPrinterMutex(printerName string, fn func()) {
	m, exist := u.printerMutex[printerName]
	if !exist {
		m = new(sync.Mutex)
		u.printerMutex[printerName] = m
	}

	m.Lock()
	defer m.Unlock()

	fn()
}

func (u *printerUseCase) Print(request dto_request.PrinterPrintRequest) {
	u.execPrinterMutex(global.GetConfig().PrinterName, func() {
		conn, err := printer.Open(global.GetConfig().PrinterName)
		if err != nil {
			panic(err)
		}

		err = conn.StartRawDocument("document_name.txt")
		if err != nil {
			panic(err)
		}
		defer conn.Close()

		datas := []byte{}
		for _, val := range request.PrinterData {
			datas = append(datas, byte(val))
		}

		_, err = conn.Write([]byte(datas))
		if err != nil {
			panic(err)
		}
	})
}

func NewPrinterUseCase() PrinterUseCase {
	return &printerUseCase{
		printerMutex: make(map[string]*sync.Mutex),
	}
}
