package manager

import (
	"setia-abadi-printer/use_case"
)

type UseCaseManager interface {
	PrinterUseCase() use_case.PrinterUseCase
}

type useCaseManager struct {
	printerUseCase use_case.PrinterUseCase
}

func (u *useCaseManager) PrinterUseCase() use_case.PrinterUseCase {
	return u.printerUseCase
}

func newUseCaseManager() *useCaseManager {
	return &useCaseManager{
		printerUseCase: use_case.NewPrinterUseCase(),
	}
}
