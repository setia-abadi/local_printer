package manager

import (
	"strings"
)

type Container struct {
	exePath        string
	useCaseManager *useCaseManager
}

func (c *Container) UseCaseManager() UseCaseManager {
	return c.useCaseManager
}

func (c *Container) ExePath() string {
	return c.exePath
}

func NewContainer(exePath string) *Container {
	useCaseManager := newUseCaseManager()

	realPath := strings.Join(strings.Split(exePath, "\\")[:len(strings.Split(exePath, "\\"))-1], "/")

	return &Container{
		exePath:        realPath,
		useCaseManager: useCaseManager,
	}
}
