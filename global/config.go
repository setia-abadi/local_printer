package global

import (
	"fmt"
	"os"
)

const (
	EnvironmentProduction  = "production"
	EnvironmentDevelopment = "development"
)

type GlobalConfig struct {
	BaseDir            string
	Environment        string
	Port               int
	PrinterName        string
	Uri                string
	CorsAllowedOrigins []string
	ServiceName        string
}

var config GlobalConfig

func init() {
	if err := LoadConfig(); err != nil {
		panic(err)
	}
}

func LoadConfig() error {
	dir, err := os.Getwd()
	if err != nil {
		return err
	}
	config.BaseDir = dir

	config.CorsAllowedOrigins = []string{"*"}
	config.Environment = "production"
	config.ServiceName = "setia-abadi-printer"

	return nil
}

func GetConfig() GlobalConfig {
	return config
}

func GetBaseDir() string {
	return config.BaseDir
}

func SetEnvironment(environment string) {
	for _, val := range []string{EnvironmentProduction, EnvironmentDevelopment} {
		if val == environment {
			config.Environment = environment
		}
	}
}

func SetPort(port int) {
	config.Port = port
	config.Uri = fmt.Sprintf("http://localhost:%d", port)
}

func SetPrinterName(printerName string) {
	config.PrinterName = printerName
}

func IsProduction() bool {
	return config.Environment == EnvironmentProduction
}

func IsDevelopment() bool {
	return config.Environment == EnvironmentDevelopment
}
