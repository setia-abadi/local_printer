package dto_response

var (
	ErrInvalidSignature        = NewBadRequestErrorResponse("invalid signature")
	ErrMissingSignature        = NewBadRequestErrorResponse("X-Signature not provided")
	ErrPrinterNameNotAvailable = NewBadRequestErrorResponse("Printer not available")
	ErrSignatureExpired        = NewBadRequestErrorResponse("signature expired")
)
