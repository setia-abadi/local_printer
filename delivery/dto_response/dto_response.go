package dto_response

import (
	"net/http"
)

type Response struct {
	Data interface{} `json:"data"`
}

type SuccessResponse struct {
	Message string `json:"message" example:"OK"`
}

type Error struct {
	Domain  string `json:"domain"`
	Message string `json:"message"`
}

type ErrorResponse struct {
	Code    int     `json:"code"`
	Message string  `json:"message"`
	Errors  []Error `json:"errors"`
}

type DataResponse map[string]interface{}

type PaginationResponse struct {
	Total int         `json:"total" example:"24"`
	Page  *int        `json:"page" example:"1"`
	Limit *int        `json:"limit" example:"10"`
	Nodes interface{} `json:"nodes"`
}

func NewUnauthorizedErrorResponse(message string) ErrorResponse {
	return ErrorResponse{
		Code:    http.StatusUnauthorized,
		Message: message,
		Errors:  []Error{},
	}
}

func NewUnauthorizedErrorResponseP(message string) *ErrorResponse {
	r := NewUnauthorizedErrorResponse(message)
	return &r
}

func NewBadRequestErrorResponse(message string) ErrorResponse {
	return ErrorResponse{
		Code:    http.StatusBadRequest,
		Message: message,
		Errors:  []Error{},
	}
}

func NewBadRequestErrorResponseP(message string) *ErrorResponse {
	r := NewBadRequestErrorResponse(message)
	return &r
}

func NewForbiddenErrorResponse(message string) ErrorResponse {
	return ErrorResponse{
		Code:    http.StatusForbidden,
		Message: message,
		Errors:  []Error{},
	}
}

func NewForbiddenErrorResponseP(message string) *ErrorResponse {
	r := NewForbiddenErrorResponse(message)
	return &r
}

func NewNotFoundErrorResponse(message string) ErrorResponse {
	return ErrorResponse{
		Code:    http.StatusNotFound,
		Message: message,
		Errors:  []Error{},
	}
}

func NewNotFoundErrorResponseP(message string) *ErrorResponse {
	r := NewNotFoundErrorResponse(message)
	return &r
}

func NewInternalServerErrorResponse() ErrorResponse {
	return ErrorResponse{
		Code:    http.StatusInternalServerError,
		Message: "Internal server error",
		Errors:  []Error{},
	}
}

func NewInternalServerErrorResponseP() *ErrorResponse {
	r := NewInternalServerErrorResponse()
	return &r
}
