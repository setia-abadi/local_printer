package dto_request

type PrinterPrintRequest struct {
	PrinterData []int16 `json:"printer_data"`
}
