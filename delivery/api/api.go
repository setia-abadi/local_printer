package api

import (
	"context"
	"encoding/json"
	"net/http"
	"setia-abadi-printer/delivery/dto_response"
	"setia-abadi-printer/global"
	"setia-abadi-printer/manager"
	"setia-abadi-printer/middleware"
	"setia-abadi-printer/util"
	"time"

	bindingInternal "setia-abadi-printer/internal/gin/binding"
	"setia-abadi-printer/internal/gin/validator"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type apiContext struct {
	ginCtx *gin.Context
}

func newApiContext(ctx *gin.Context) apiContext {
	return apiContext{
		ginCtx: ctx,
	}
}

func (a *apiContext) context() context.Context {
	return a.ginCtx.Request.Context()
}

func (a *apiContext) mustBind(obj interface{}) {
	if err := a.shouldBind(obj); err != nil {
		panic(a.translateBindErr(err))
	}
}

func (a *apiContext) shouldBind(obj interface{}) error {
	return util.ShouldGinBind(a.ginCtx, obj)
}

func (a *apiContext) translateBindErr(err error) dto_response.ErrorResponse {
	var r dto_response.ErrorResponse

	switch v := err.(type) {
	case validator.ValidationErrors:
		errs := []dto_response.Error{}
		translations := v.Translate(a.ginCtx)
		for k, translation := range translations {
			errs = append(errs, dto_response.Error{
				Domain:  k,
				Message: translation,
			})
		}

		r = dto_response.NewBadRequestErrorResponse("Invalid request payload")
		r.Errors = errs

	case *json.UnmarshalTypeError, *json.InvalidUnmarshalError:
		r = dto_response.NewBadRequestErrorResponse("Invalid request payload")

	default:
		switch v {
		case bindingInternal.ErrConvertMapStringSlice, bindingInternal.ErrConvertToMapString,
			bindingInternal.ErrMultiFileHeader, bindingInternal.ErrMultiFileHeaderLenInvalid:
			r = dto_response.NewBadRequestErrorResponse("Invalid request payload")

		default:
			panic(err)
		}
	}

	return r
}

func (a *apiContext) json(code int, obj interface{}) {
	a.ginCtx.JSON(code, obj)
}

func (a *apiContext) getSignature() string {
	return middleware.GetSignature(a.context())
}

type api struct {
}

func newApi() api {
	return api{}
}

func (a *api) Wrapper(fn func(apiCtx apiContext)) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		apiCtx := newApiContext(ctx)
		fn(apiCtx)
	}
}

func NewRouter(container *manager.Container) *gin.Engine {
	if global.IsProduction() {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.New()
	router.Use(cors.New(cors.Config{
		AllowOrigins: global.GetConfig().CorsAllowedOrigins,
		AllowMethods: []string{
			http.MethodGet,
			http.MethodPost,
		},
		AllowHeaders: []string{
			"Accept",
			"Accept-Encoding",
			"Authorization",
			"Cache-Control",
			"Content-Type",
			"Content-Length",
			"Origin",
			"X-CSRF-Token",
			"X-Requested-With",
			"X-Signature",
		},
		ExposeHeaders: []string{
			"Content-Length",
		},
		AllowCredentials: true,
		MaxAge:           2 * time.Hour,
	}))

	registerMiddlewares(router, container)
	registerRoutes(router, container)

	return router
}

func registerRoutes(router *gin.Engine, container *manager.Container) {
	RegisterPrinterApi(router, container.UseCaseManager())
}

func registerMiddlewares(router gin.IRouter, container *manager.Container) {
	middleware.PanicHandlers(router, container)
	middleware.SignatureMiddleware(router)
}
