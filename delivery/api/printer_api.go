package api

import (
	"net/http"
	"setia-abadi-printer/delivery/dto_request"
	"setia-abadi-printer/delivery/dto_response"
	"setia-abadi-printer/manager"
	"setia-abadi-printer/use_case"

	"github.com/gin-gonic/gin"
)

type PrinterApi struct {
	api
	printerUseCase use_case.PrinterUseCase
}

// @Router		/printers [get]
// @Summary	Get Printer List
// @tags		Printers
// @Accept		json
// @Produce	json
// @Success	200	{object}	dto_response.Response{data=dto_response.DataResponse{printers=[]string}}
func (a *PrinterApi) GetPrinterList() gin.HandlerFunc {
	return a.Wrapper(func(ctx apiContext) {
		printerList := a.printerUseCase.GetPrinterList()

		ctx.json(http.StatusOK, dto_response.Response{
			Data: dto_response.DataResponse{
				"printers": printerList,
			},
		})

	})
}

// @Router		/printers [post]
// @Summary	Printers
// @tags		Printers
// @Accept		json
// @Param		dto_request.PrinterData	body	dto_request.PrinterData	true	"Request Body"
// @Produce	json
// @Success	200	{object}	dto_response.SuccessResponse
func (a *PrinterApi) Print() gin.HandlerFunc {
	return a.Wrapper(func(ctx apiContext) {
		var request dto_request.PrinterPrintRequest
		ctx.mustBind(&request)

		a.printerUseCase.Print(request)

		ctx.json(http.StatusOK, dto_response.SuccessResponse{
			Message: "OK",
		})
	})
}

func RegisterPrinterApi(router gin.IRouter, useCaseManager manager.UseCaseManager) {
	api := PrinterApi{
		api:            newApi(),
		printerUseCase: useCaseManager.PrinterUseCase(),
	}

	routerGroup := router.Group("/printers")
	routerGroup.GET("", api.GetPrinterList())
	routerGroup.POST("", api.Print())
}
