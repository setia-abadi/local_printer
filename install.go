// Copyright 2012 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build windows
// +build windows

package main

import (
	"fmt"
	"setia-abadi-printer/util"
	"time"

	"golang.org/x/sys/windows"
	"golang.org/x/sys/windows/svc/mgr"
)

func installService(name, desc, port, printerName string) error {
	util.LogActivity(fmt.Sprintf("Install %s service.", name))

	exepath, err := util.ExePath()
	if err != nil {
		return err
	}
	m, err := mgr.Connect()
	if err != nil {
		return err
	}
	defer m.Disconnect()
	s, err := m.OpenService(name)
	if err == nil {
		s.Close()
		return fmt.Errorf("service %s already exists", name)
	}
	s, err = m.CreateService(name, exepath, mgr.Config{
		ServiceType:      windows.SERVICE_WIN32_OWN_PROCESS,
		StartType:        mgr.StartAutomatic,
		DisplayName:      name,
		Description:      desc,
		DelayedAutoStart: true,
	}, port, printerName)
	if err != nil {
		return err
	}
	defer s.Close()

	s.SetRecoveryActions([]mgr.RecoveryAction{
		{
			Type:  mgr.ServiceRestart,
			Delay: 3 * time.Second,
		},
	}, 86400)

	return nil
}

func removeService(name string) error {
	util.LogActivity(fmt.Sprintf("Remove %s service.", name))

	m, err := mgr.Connect()
	if err != nil {
		return err
	}
	defer m.Disconnect()
	s, err := m.OpenService(name)
	if err != nil {
		return fmt.Errorf("service %s is not installed", name)
	}
	defer s.Close()
	err = s.Delete()
	if err != nil {
		return err
	}
	return nil
}
