package constant

import "time"

const (
	SignatureValidDuration = time.Minute // 1 Minute
)
